package com.example.bs23_project.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bs23_project.databinding.LayoutOfListRepoBinding
import com.example.bs23_project.models.Item

class RepoAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>()  {
    private val list : ArrayList<Item> = arrayListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PrepaidCardTransactionViewHolder(
            LayoutOfListRepoBinding.inflate(
                parent.layoutInflater,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PrepaidCardTransactionViewHolder).bind(position)
    }

    private inner class PrepaidCardTransactionViewHolder(private val binding: LayoutOfListRepoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.apply {
                list[position].let {

                }

            }
        }
    }

    fun setDataToAdapter(list : ArrayList<Item>){
        this.list.addAll(list)
        notifyItemRangeChanged(this.list.size,list.size)
    }
}
