package com.example.bs23_project.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bs23_project.data.network.Response
import com.example.bs23_project.data.repository.SearchRepository
import com.example.bs23_project.models.SearchModelClass
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repository: SearchRepository
) : ViewModel() {
    val cityLiveData: MutableLiveData<Response<SearchModelClass>>
        get() = repository.repoDat


//    init {
//        viewModelScope.launch (Dispatchers.IO){
//
//        }
//    }

    suspend fun getRepoData(){
        repository.getRepository()
    }
}