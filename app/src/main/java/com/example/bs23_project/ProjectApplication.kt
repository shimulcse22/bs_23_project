package com.example.bs23_project

import android.app.Application
import com.example.bs23_project.di.ApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerAppCompatDialogFragment
import dagger.android.support.DaggerApplication

class ProjectApplication: Application() {
    lateinit var applicationComponent: ApplicationComponent

    //    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
//        return DaggerAppComponent.factory().create(this)
//    }
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.factory().create(this)
    }

}