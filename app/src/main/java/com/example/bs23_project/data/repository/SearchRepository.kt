package com.example.bs23_project.data.repository

import androidx.lifecycle.MutableLiveData
import com.example.bs23_project.data.network.ApiService
import com.example.bs23_project.data.network.Response
import com.example.bs23_project.models.SearchModelClass
import com.example.bs23_project.utils.Constants

import java.lang.Exception
import javax.inject.Inject

class SearchRepository @Inject constructor(
    private val apiService: ApiService
) {
    private val _repoData = MutableLiveData<Response<SearchModelClass>>()
    val repoDat : MutableLiveData<Response<SearchModelClass>>
        get() = _repoData

    suspend fun getRepository(){
        try {
            val result = apiService.getApiData(Constants.REPO_PARAM)
            if(result.isSuccessful && result.body()!=null){
                _repoData.postValue(Response.Success(result.body()))
            }else{
                _repoData.postValue(Response.Error(result.errorBody().toString()))
            }
        }catch (e:Exception){
            _repoData.postValue(Response.Error(e.message.toString()))
        }
    }
}