package com.example.bs23_project.data.network

import com.example.bs23_project.models.SearchModelClass
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("/search/repositories")
    suspend fun getApiData(@Query("q") params: String?) : Response<SearchModelClass>
}