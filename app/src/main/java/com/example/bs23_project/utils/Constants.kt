package com.example.bs23_project.utils

object Constants {
    const val BASE_URL = "https://api.github.com/"
    const val REPO_PARAM = "android"
}