package com.example.bs23_project.di

import android.content.Context
import com.example.bs23_project.MainActivity
import com.example.bs23_project.ProjectApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class])
interface ApplicationComponent : AndroidInjector<ProjectApplication> {
    fun inject(searchActivity: MainActivity)

    @Component.Factory
    abstract class Factory : AndroidInjector.Factory<ProjectApplication>
}