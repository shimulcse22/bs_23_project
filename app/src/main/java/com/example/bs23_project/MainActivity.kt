package com.example.bs23_project

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bs23_project.data.network.Response
import com.example.bs23_project.databinding.ActivityMainBinding
import com.example.bs23_project.ui.RepoAdapter
import com.example.bs23_project.viewmodel.AppViewModelFactory
import com.example.bs23_project.viewmodel.MainViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    lateinit var mainViewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter : RepoAdapter

    @Inject
    lateinit var appViewModelFactory: AppViewModelFactory
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainViewModel = ViewModelProvider(this, appViewModelFactory)[MainViewModel::class.java]
        binding.viewMOdel = mainViewModel
        binding.lifecycleOwner = this

        adapter = RepoAdapter()

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this)

//        lifecycleScope.launch{
//            mainViewModel
//        }
        GlobalScope.launch {
            mainViewModel.getRepoData()
        }

        mainViewModel.cityLiveData.observe(this) {
            when (it) {
                is Response.Loading -> {
                    //binding.loading.visibility = View.VISIBLE
                }
                is Response.Success -> {
                    //binding.loading.visibility = View.GONE
                    //moveToOtherActivity()
                }
                is Response.Error -> {
                    //binding.loading.visibility = View.GONE
                    Toast.makeText(this, it.error, Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}